--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Empresa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Empresa" (
    id integer NOT NULL,
    nombre text,
    rif text,
    fecha_apertura date,
    fecha_creacion date,
    creado_por integer,
    actualizado_por integer,
    fecha_actualizacion date,
    estatus integer
);


ALTER TABLE "Empresa" OWNER TO postgres;

--
-- Name: Empresa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Empresa_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Empresa_id_seq" OWNER TO postgres;

--
-- Name: Empresa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Empresa_id_seq" OWNED BY "Empresa".id;


--
-- Name: Usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Usuario" (
    id integer NOT NULL,
    nombre text,
    apellido text,
    fecha_nacimiento date,
    email text,
    clave text,
    cedula text,
    empresa_id integer,
    fecha_creacion date,
    creado_por integer,
    actualizado_por integer,
    fecha_actualizacion integer NOT NULL,
    estatus integer
);


ALTER TABLE "Usuario" OWNER TO postgres;

--
-- Name: Usuario_fecha_actualizacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Usuario_fecha_actualizacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Usuario_fecha_actualizacion_seq" OWNER TO postgres;

--
-- Name: Usuario_fecha_actualizacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Usuario_fecha_actualizacion_seq" OWNED BY "Usuario".fecha_actualizacion;


--
-- Name: Usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Usuario_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Usuario_id_seq" OWNER TO postgres;

--
-- Name: Usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Usuario_id_seq" OWNED BY "Usuario".id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Empresa" ALTER COLUMN id SET DEFAULT nextval('"Empresa_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuario" ALTER COLUMN id SET DEFAULT nextval('"Usuario_id_seq"'::regclass);


--
-- Name: fecha_actualizacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuario" ALTER COLUMN fecha_actualizacion SET DEFAULT nextval('"Usuario_fecha_actualizacion_seq"'::regclass);


--
-- Data for Name: Empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Empresa" (id, nombre, rif, fecha_apertura, fecha_creacion, creado_por, actualizado_por, fecha_actualizacion, estatus) FROM stdin;
1	empresa1	rif01	2015-01-15	2015-01-15	1	1	2015-01-15	1
\.


--
-- Name: Empresa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Empresa_id_seq"', 1, true);


--
-- Data for Name: Usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Usuario" (id, nombre, apellido, fecha_nacimiento, email, clave, cedula, empresa_id, fecha_creacion, creado_por, actualizado_por, fecha_actualizacion, estatus) FROM stdin;
\.


--
-- Name: Usuario_fecha_actualizacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Usuario_fecha_actualizacion_seq"', 1, false);


--
-- Name: Usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Usuario_id_seq"', 1, false);


--
-- Name: Empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Empresa"
    ADD CONSTRAINT "Empresa_pkey" PRIMARY KEY (id);


--
-- Name: Empresa_rif_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Empresa"
    ADD CONSTRAINT "Empresa_rif_key" UNIQUE (rif);


--
-- Name: Usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Usuario"
    ADD CONSTRAINT "Usuario_pkey" PRIMARY KEY (id);


--
-- Name: fki_usuario_empresa; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_usuario_empresa ON "Usuario" USING btree (empresa_id);


--
-- Name: usuario_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuario"
    ADD CONSTRAINT usuario_empresa FOREIGN KEY (empresa_id) REFERENCES "Empresa"(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

