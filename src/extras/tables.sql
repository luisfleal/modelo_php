CREATE DATABASE phalcon_practices;

\connect phalcon_practices;

CREATE TABLE genericos (
    id          char(5),
    nombre      varchar(40) NOT NULL UNIQUE,
    tipo        integer NOT NULL,
    fecha       timestamp DEFAULT current_timestamp,
    marca       varchar(10),
    CONSTRAINT cons_tipo_mayor_cero_y_marca_no_vacio CHECK (tipo > 0 AND marca <> ''),
    PRIMARY KEY(id)
);

-- execute with: 
-- psql -d postgres -a -f base/extras/tables.sql
