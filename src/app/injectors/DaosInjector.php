<?php

/**
    Clase que se encarga de inyectar todos los daos existentes en el
    componente Phalcon de gestion de dependencias
*/
class DaosInjector
{
    private static $instance = NULL;
    public $logger = NULL;
    public $di = NULL;

    /**
        Recibe una referencia del logger y del FactoryDefault ($di)
    */
    public function __construct ($logger, $di)
    {
        $this->logger = $logger;
        $this->di = $di;
        $this->init();
    }

    /**
        Funcion invocada desde el constructor que se encarga de obtener el singleton
        de cada dao y realizar la inyeccion de la dependencia
    */
    public function init()
    {

        $this->logger->debug("injections of daos");

       /* $this->di->set('genericDao', function() {
            return GenericDao::getInstance($this->logger);

        });*/
        $this->di->set('empresaDao', function() {
            return EmpresaDao::getInstance($this->logger);

        });
        $this->di->set('usuarioDao', function() {
            return UsuarioDao::getInstance($this->logger);

        });
    }

    public static function getInstance($logger, $di){
        if(is_null(self::$instance))
            self::$instance = new DaosInjector($logger, $di);
        return self::$instance;
    }
}
