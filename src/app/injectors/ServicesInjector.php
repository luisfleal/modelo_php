<?php

/**
    Clase que se encarga de inyectar todos los servicios existentes en el
    componente Phalcon de gestion de dependencias
*/
class ServicesInjector
{
    private static $instance = NULL;
    public $logger = NULL;
    public $di = NULL;

    /**
        Recibe una referencia del logger y del FactoryDefault ($di)
    */
    public function __construct ($logger, $di)
    {
        $this->logger = $logger;
        $this->di = $di;
        $this->init();
    }

    /**
        Funcion invocada desde el constructor que se encarga de obtener el singleton
        de cada servicio y realizar la inyeccion de la dependencia
        En el proceso de inyeccion del service a la nube de dependencias, se obtiene una
        referencia del dao del que depende este service
    */
    public function init()
    {
        $this->logger->debug("injections of services");

        /*$this->di->set('genericService', function()  {
            return GenericService::getInstance($this->logger, $this->di->get('genericDao'));
        });*/
        $this->di->set('empresaService', function()  {
            return EmpresaService::getInstance($this->logger, $this->di->get('empresaDao'));
        });
        $this->di->set('usuarioService', function()  {
            return UsuarioService::getInstance($this->logger, $this->di->get('usuarioDao'));
        });
    }

    public static function getInstance($logger, $di){
        if(is_null(self::$instance))
            self::$instance = new ServicesInjector($logger, $di);
        return self::$instance;
    }
}




