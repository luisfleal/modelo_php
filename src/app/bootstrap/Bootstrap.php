<?php

use Phalcon\Loader,
    Phalcon\Logger,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Logger\Adapter\File as FileAdapter,
    Phalcon\Config\Adapter\Ini as ConfigIni,
    Phalcon\Db\Adapter\Pdo\PostgreSQL;

/**
    Clase realiza la carga inicia de todas las clases de negocio,
    configura el logger del sistema, crea conexion a BD y realiza
    la carga inicial de los services y daos inyectandolos a la 
    micro aplicacion
*/
class Bootstrap
{
    private static $instance = NULL;
    public $logger = NULL;
    public $di = NULL;
    public $app = NULL;
    public $loader = NULL;
    
    public function __construct($app)
    {
        $this->app = $app;
        $this->di = $app->di; // se obtiene una referencia del inyector de dependencias
        $this->init();
    }

    /**
        Funcion principal que inicia toda la carga de componentes internos de la web
    */
    public function init()
    {
        $config = new ConfigIni(ICommons::CONFIG_FILE);

        $this->logger = new FileAdapter($config->common->logsPath);
        $this->logger->setLogLevel(Logger::DEBUG);
        $this->logger->debug("Logger configurado");
        
        $this->loader = new Loader();

        $this->loadDBConnection($config);

        $this->loadGlobalDirs($config);
        
        $this->loadClasses();

        $this->loadInjectors();

        $this->loadControllers();
        
        $this->beforeRoutesExecutions();
        $this->afterRoutesExecutions();
        $this->finishRoutesExecutions();

        $this->versionInfoDisplay($config);
    }

    /**
        Funcion que define un path simple para informar la version del API y la fecha del build
    */
    private function versionInfoDisplay($config){
        $this->logger->debug("Definiendo ruta version");
        $this->app->get('/version/info',function() use ($config){
            $this->logger->debug("Retornando version");
            $this->app->response->setStatusCode(ICommons::HTTP_200, ICommons::HTTP_200_MSG);
            $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
            $this->app->response->setJsonContent(
                array(
                    ICommons::RESPONSE_RESULTADO => ICommons::HTTP_200_MSG, 
                    ICommons::RESPONSE_VERSION => $config->common->version,
                    ICommons::RESPONSE_VERSION_FECHA => $config->common->versionDate
                )
            );
            $this->app->response->send();
        });

    }
    
    /**
        Funcion de uso comun para validar el header content-type para todos las request
        Se pueden incluir más validaciones que sean comunes a todas las rutas, por ejemplo autenticacion
    */

    private function beforeRoutesExecutions()
    {
        
        $this->app->before(function () {
            
            if ( ($this->app->request->isPost() || $this->app->request->isPut()) and strcasecmp($this->app->request->getHeader(ICommons::HEADER_CONTENT_TYPE), ICommons::HEADER_CONTENT_JSON) !== 0) 
            {
                $this->logger->debug("Validando para peticions post o put content type = ".$this->app->request->getHeader(ICommons::HEADER_CONTENT_TYPE));
                // al recibirse un content-type distinto a json/application se rechaza la peticion
                $this->app->response->setStatusCode(ICommons::HTTP_400, ICommons::HTTP_400_MSG);
                $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_400_MSG, ICommons::RESPONSE_MENSAJE => ICommons::HEADER_CONTENT_TYPE_INVALID));
                $jsonBody = json_encode(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_400_MSG, ICommons::RESPONSE_MENSAJE => ICommons::HEADER_CONTENT_TYPE_INVALID));
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_LENGTH,strlen($jsonBody));
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_MD5,base64_encode(md5($jsonBody, true)));
                $this->app->response->send();
                return false;
            }
            return true;
        });
    }
    /**
        Funcion de uso comun que añade el content-type application/json a todas las peticiones, el content-length
        y crea un checksum usando md5 de todo el body response. Añade estos valores al header de la peticion
    */
    
    private function afterRoutesExecutions(){
        $this->app->after(function () {
            $this->logger->debug("Setting content length and md5");
            $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON); // set de content-type json/application
            $this->app->response->setHeader(ICommons::HEADER_CONTENT_LENGTH,strlen($this->app->getReturnedValue())); // set de content-length
            $this->app->response->setHeader(ICommons::HEADER_CONTENT_MD5,base64_encode(md5($this->app->getReturnedValue(), true))); // set de content-md5
            $this->app->response->send();
        });
        
    }

    /**
        Funcion de uso comun que ejecuta sus instrucciones luego que es despachada la peticion
    */
    private function finishRoutesExecutions(){
        // TO-DO: possible place for Bitacora log?
    }
    
    /**
        Function para cargar clases de forma específica
    */
    private function loadClasses()
    {
        $this->logger->info("Cargando clases especificas");
        $this->loader->registerClasses(
            array(
                "Collection" => "../app/commons/Collection.php",
            )
        );
    }
    
    /**
        Function para carga de directorios en el class loader
    */
    private function loadGlobalDirs($config) 
    {
        $this->logger->info("Cargando clases base");
        
        $this->loader->registerDirs(array(
            $config->phalcon->validators,
            $config->phalcon->models,
            $config->phalcon->daos,
            $config->phalcon->services,
            $config->phalcon->injectors,
            $config->phalcon->routes,
            $config->phalcon->controllers
        ))->register();
        /** 
            Es muy importante cargar los directorios en orden de dependencia
            Tambien es importante tener en mente que Phalcon realizara la carga
            de estos directorios revisando uno a uno cada archivo y comparando
            la definicion de clases en ellos para evitar solapamientos. Esto puede
            penalizar en rendimiento. Hay que ser cuidadoso
        */
    }
    
    /**
    Carga la conexion a la BD usando los properties definidos en el archivo .ini
    */
    private function loadDBConnection($config) 
    {
        $this->logger->info("Creando conexion de base de datos");
        $this->di->set('db',function () use ($config) {

            $eventsManager = new EventsManager();
            // Realiza logs de todas las operaciones de BD
            $eventsManager->attach('db', function ($event, $connection) {
                if ($event->getType() == 'beforeQuery') {
                    $this->logger->log($connection->getSQLStatement(), Logger::INFO);
                }
            });
            $connection = new PostgreSQL((array)$config->database);
            
            $connection->setEventsManager($eventsManager);
            
            return $connection;
            
        });
    }

    private function loadInjectors()
    {
        /** 
            En estas dos clases deben definirse las instancias de cada dao
            y service y realizar su inyeccion a arbol de dependencias de phalcon
            Cada instancia e inyeccion se realiza respectivamente en cada clase
        */
        DaosInjector::getInstance($this->logger, $this->di);
        ServicesInjector::getInstance($this->logger, $this->di);
        
    }
    /**
        Instancias de los contollers
        Esto se debe repetir en esta funcion para controller que se necesite
        Internamente, en la construccion del controlador, se hace invocacion
        de la funcion que define y carga todas las rutas de dicho controller
        @see GenericRouterDefiner
    */
    private function loadControllers()
    {
       // GenericController::getInstance($this->logger,$this->app->genericService,$this->app);
        EmpresaController::getInstance($this->logger,$this->app->empresaService,$this->app);
        UsuarioController::getInstance($this->logger,$this->app->usuarioService,$this->app);
    }

    public static function getInstance($app){
        if(is_null(self::$instance))
            self::$instance = new Bootstrap($app);
        return self::$instance;
    }
    
    public static function getExistInstance(){
        if(!is_null(self::$instance))
            return self::$instance;
        else
            return NULL;
    }
    
    public function getLogger(){
        return $this->logger;
    }
}


