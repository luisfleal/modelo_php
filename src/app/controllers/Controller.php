<?php
/**
    Controlador base para toda la jerarquia de controllers del sistema
*/
class Controller{

    private static $instance = NULL;
    protected $logger = NULL;
    private $app = NULL;
    
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
        Las siguientes son funciones genericas que pueden reutilizarse en los demas controladores
        Si se añaden más funciones, deben añadirse en orden en funcion de status code
    */
    
    public function responseOK($data){
        $this->app->response->setStatusCode(ICommons::HTTP_200, ICommons::HTTP_200_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_200_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
    
    public function responseCreatedOK($data){
        $this->app->response->setStatusCode(ICommons::HTTP_201, ICommons::HTTP_201_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_201_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
    
    public function responseBadRequest($data){
        $this->app->response->setStatusCode(ICommons::HTTP_400, ICommons::HTTP_400_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_400_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
    
    public function responseForbidden($data){
        $this->app->response->setStatusCode(ICommons::HTTP_403, ICommons::HTTP_403_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_403_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
    
    public function responseNotFound($data){
        $this->app->response->setStatusCode(ICommons::HTTP_404, ICommons::HTTP_404_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_404_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
    
    public function responseInternalServerError($data){
        $this->app->response->setStatusCode(ICommons::HTTP_500, ICommons::HTTP_500_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_500_MSG, ICommons::RESPONSE_MENSAJE => $data));
    }
}

?>
