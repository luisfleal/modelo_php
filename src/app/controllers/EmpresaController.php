<?php
/**
    Controlador ejemplo que hereda de Controller quien tienen definiciones basicas
    para la gestion de http status code asi como la jsonificacion de las respuestas
*/
class EmpresaController extends Controller
{
    private static $instance = NULL;
    private $service = NULL;
    public $logger = NULL;
    public $app = NULL;

    /**
        Este controlador recibe la instancia de loger, la implementacion del service a usar
        y la app que es necesaria para definir las rutas
        @param $logger -> instancia unica del logger definida en el index
        @param IGenericService $service -> se crea un objeto de tipo interfaz para que pueda
            usarse la instancia que sea necesaria en el momento de la inyeccion
        @param app -> contexto de la microapp, necesario para definir las rutas
    */
    public function __construct($logger, IEmpresaService $service, $app) {
        $this->logger = $logger;
        $this->service = $service;
        $this->app = $app;
        parent::__construct( $app ); // pasamos a la clase padre la app
        $this->loadRoutes();
    }
    
    /**
        Funcion que invoca al objeto que define las rutas para este controlador
    */
    public function loadRoutes()
    {
        EmpresaRouterDefiner::getInstance($this);
    }

    public function listar()
    {
        $filter = $this->filterValidator();
        
        $data = $this->service->listar($filter);
        $this->logger->debug("list on controller");
        // se puede configurar a mano la respuesta:
        $this->app->response->setStatusCode(ICommons::HTTP_200, ICommons::HTTP_200_MSG);
        $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_200_MSG, ICommons::RESPONSE_MENSAJE => $data));
        // o se puede usar una opcion comun para el resultado OK:
        // $this->responseOK($data);
        // o una respuesta de error
        // $this->responseBadRequest($data);
    }
    
    private function filterValidator()
    {
        // los siguientes 3 deben ser comunes en las listas
        $order = $this->app->request->getQuery('order');
        $limit = $this->app->request->getQuery('limit');
        $offset = $this->app->request->getQuery('offset');
        
        $filter = array();
        
        if (!empty($order)) 
        {
            $filter["order"] = $order;
        }
        
        if (!empty($limit)) 
        {
            $filter["limit"] = $limit;
        }
        
        if (!empty($offset)) 
        {
            $filter["offset"] = $offset;
        }
        
        
        $q = $this->app->request->getQuery('q');        // opcional si hay búsquedas
        $nombre = $this->app->request->getQuery('nombre');   // opcional para filtrado de campos propios de la entidad    

        if (!empty($q)) 
        {
            $filter["campo"] = $q;
        }

        if (!empty($nombre)) 
        {
            $filter["nombre"] = $nombre;
        }        
        
        return $filter;
    }
    
    public function nuevo(){
        $object = $this->app->request->getJsonRawBody();
        $data = $this->service->nuevo($object);
        if ($data) {
            if (gettype($data) == "string"){
                if (strpos($data,ICommons::ERROR) !== false) {
                    $this->responseBadRequest($data);
                } else {
                    $this->responseCreatedOK($data);
                }                
            } else {
                $this->responseCreatedOK($data);
            }
        } else 
        {
            $this->responseBadRequest($data);
        }

    }

    public function get($id){
        if (!is_numeric($id)) 
        {
            $this->responseBadRequest(ICommons::INVALID_FILTER);
            return;
        }
        $data = $this->service->getByPK($id);
        if ($data) 
        {
            if (gettype($data) == "string"){
                if (strpos($data,ICommons::ERROR) !== false) {
                    $this->responseNotFound($data);
                } else {
                    $this->responseOK($data);
                }                
            } else {
                $this->responseOK($data);
            }
        } else 
        {
            $this->responseNotFound($data);
        }
    }

    public function modificar($id){
        if (!is_numeric($id)) 
        {
            $this->responseBadRequest(ICommons::INVALID_FILTER);
            return;
        }
        $body = $this->app->request->getJsonRawBody();
        $data = $this->service->modificar($id, $body);
        if ($data) {
            if (gettype($data) == "string"){
                if (strpos($data,ICommons::ERROR) !== false) {
                    $this->responseBadRequest($data);
                } else {
                    $this->responseOK($data);
                }                
            } else {
                $this->responseOK($data);
            }
        } else 
        {
            $this->responseBadRequest($data);
        }
    }

    public function eliminar($id){
        if (!is_numeric($id)) 
        {
            $this->responseBadRequest(ICommons::INVALID_FILTER);
            return;
        }
        $data = $this->service->eliminar($id);
        $this->responseOK($data);
    }

    public static function getInstance($logger, IEmpresaService $empresaService, $app){
        if (is_null(self::$instance)) {
            self::$instance = new EmpresaController($logger, $empresaService, $app);
        }
        return self::$instance;
    }

}

