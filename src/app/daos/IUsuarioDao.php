<?php
/**
    Interfaz generica para definir los metodos del dao
*/
interface IUsuarioDao
{

    public function listar($filter);

    public function nuevo($usuario);

    public function modificar($usuario);
    
    public function eliminar($usuario);
    
    public function findByPK($pk);


}
