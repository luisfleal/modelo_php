<?php
/**
    Interfaz generica para definir los metodos del dao
*/
interface IGenericDao
{

    public function listar($filter);

    public function nuevo($generic);

    public function modificar($generic);
    
    public function eliminar($generic);
    
    public function findByPK($pk);


}
