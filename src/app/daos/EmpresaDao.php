<?php
use Phalcon\Mvc\User\Component;

/**
    Clase de tipo Component que implementa la interfaz que define su comportamiento
*/
class EmpresaDao extends Component implements IEmpresaDao
{
    private static $instance = NULL;

    private $logger = NULL;

    public function __construct($logger) {
        $this->logger = $logger;
    }
    
    public function findByPK($pk) 
    {
        $this->logger->debug("buscando por PK : " . $pk);
        try 
        {
            $empresa = Empresa::findFirst([
                "id = ?0",
                "bind" => [$pk]
            ]);
            return $empresa;
        }
        catch (Phalcon\Mvc\Model\Exception $ex) // se capturan todos los posibles errores de base de datos
        {
            $this->logger->error("Error : " . $ex->getMessage());
            return ICommons::UNEXPECTED_ERROR; // retorno para casos no controlados
        }
    }

    public function modificar($empresa)
    {
        $this->logger->debug("modificacion de un registro Empresa: " . $empresa);
        try 
        {
            if ($empresa->update() == false) {
                $this->logger->debug("Errores en la actualizacion de la Empresa. Mensajes: ");
                $errorMessage = "";
                foreach ($empresa->getMessages() as $message) { // se recorren los mensajes que genera el Model en la funcion validations
                    $errorMessage = $message->__toString();

                    if (strpos($errorMessage,'Record cannot be updated because it does not exist') !== false) {
                        $errorMessage = ICommons::INVALID_RECORD_NOT_EXIST;
                    } else {
                        $errorMessage = ICommons::UNEXPECTED_ERROR . ": " . $errorMessage;
                    }
                    $this->logger->error($errorMessage);
                    return $errorMessage;
                }
            } else {
                return true;
            }
        }
        catch (PDOException $ex) // se capturan todos los posibles errores de base de datos
        {
            $this->logger->error("Error : " . $ex->getMessage());
            if (strpos($ex,'SQLSTATE[22007]') !== false) { // SQLSTATE[22007]: Invalid datetime format
                return ICommons::INVALID_DATE_FORMAT;
            } elseif (strpos($ex,'SQLSTATE[22001]') !== false) { //  SQLSTATE[22001]: String data, right truncated
                return ICommons::INVALID_DATA_LENGHT;
            } else {
                return ICommons::UNEXPECTED_ERROR; // retorno para casos no controlados
            }
        }
    }

    public function nuevo($empresa)
    {
        $this->logger->debug("creacion de nuevo registro Empresa");
        try 
        {
            if ($empresa->create() == false) {
                $this->logger->debug("Errores en la creacion de la Empresa. Mensajes: ");
                $errorMessage = "";
                foreach ($empresa->getMessages() as $message) { // se recorren los mensajes que genera el Model en la funcion validations
                    $errorMessage = $message->__toString();
                    if (strpos($errorMessage,'Record cannot be created because it already exists') !== false) {
                        $errorMessage = ICommons::INVALID_SAVE_RECORD_EXIST;
                    } else {
                        $errorMessage = ICommons::UNEXPECTED_ERROR . ": " . $errorMessage;
                    }
                    $this->logger->error($errorMessage);
                    return $errorMessage;
                }
            } else {
                return true;
            }
        }
        catch (PDOException $ex) // se capturan todos los posibles errores de base de datos
        {
            $this->logger->error("Error : " . $ex->getMessage());
            if (strpos($ex,'SQLSTATE[22007]') !== false) { // SQLSTATE[22007]: Invalid datetime format
                return ICommons::INVALID_DATE_FORMAT; // ESTA VALIDACION NO ES NECESARIA PARA CAMPOS QUE USAN SETEO DE FECHA USANDO beforeCreate o beforeUpdate
            } elseif (strpos($ex,'SQLSTATE[22001]') !== false) { //  SQLSTATE[22001]: String data, right truncated
                return ICommons::INVALID_DATA_LENGHT;
            } else {
                return ICommons::UNEXPECTED_ERROR; // retorno para casos no controlados
            }
        }
    }
    
    public function listar($filter){
        $this->logger->debug("obteniendo todos los registros de Empresa");
        try 
        {            
            if (empty($filter)) 
                return Empresa::find();
            else 
            {
                // debe validarse la presencia de cada uno para no ejecutar una
                // query mal formada o con errores
                $query = Empresa::query();
                
                if (array_key_exists('nombre', $filter))
                {
                    $query->where("nombre = :nombre:");
                    $query->bind(array("nombre" => $filter['nombre']));
                }
                    
                if (array_key_exists("order", $filter))
                    $query->order($filter['order']);
                
                if (array_key_exists("limit", $filter) and array_key_exists("offset", $filter))
                    $query->limit($filter["limit"],$filter["offset"]);
                                
                return $query->execute();
            }
            
            
        }
        catch (PDOException $ex) // se capturan todos los posibles errores de base de datos
        {
            $this->logger->error("Error : " . $ex->getMessage());
            return ICommons::UNEXPECTED_ERROR; // retorno para casos no controlados
        }
        
    }
    
    public function eliminar($empresa)
    {
        $this->logger->debug("Eliminando registros de Empresa con id: " . $empresa->id);
        try 
        {
            if ($empresa->delete() == false) 
            {
                $this->logger->debug("El registro no pudo ser borrado");
                $errorMessage = "";
                foreach ($empresa->getMessages() as $message) { // se recorren los mensajes que genera el Model en la funcion validations
                    $errorMessage = $message->__toString();
                    $this->logger->error($errorMessage);
                    $errorMessage = ICommons::UNEXPECTED_ERROR . ": " . $errorMessage;
                    return $errorMessage;
                }
            } else 
            {
                $this->logger->debug("Eliminación correcta!");
                return true;
            }
            
        }
        catch (PDOException $ex) // se capturan todos los posibles errores de base de datos
        {
            $this->logger->error("Error : " . $ex->getMessage());
            return ICommons::UNEXPECTED_ERROR; // retorno para casos no controlados
        }
        
        
    }

    public static function getInstance($logger){
        if(is_null(self::$instance))
            self::$instance = new EmpresaDao($logger);
        return self::$instance;
    }
}
