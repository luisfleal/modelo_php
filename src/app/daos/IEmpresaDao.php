<?php
/**
    Interfaz generica para definir los metodos del dao
*/
interface IEmpresaDao
{

    public function listar($filter);

    public function nuevo($empresa);

    public function modificar($empresa);
    
    public function eliminar($empresa);
    
    public function findByPK($pk);


}
