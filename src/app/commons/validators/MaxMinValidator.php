<?php

use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;
use Phalcon\Mvc\EntityInterface;

class MaxMinValidator extends Validator implements ValidatorInterface
{
    public function validate(EntityInterface $model)
    {
        $field = $this->getOption('field');

        $min   = $this->getOption('min');
        $max   = $this->getOption('max');

        $value = $model->$field;
        
        if ($value < $min || $value > $max) {
            $this->appendMessage(
                "Error: El campo $field no tiene un valor valido dentro del rango. Tiene: $value",
                $field,
                "MaxMinValidator"
            );

            return false;
        }

        return true;
    }
}