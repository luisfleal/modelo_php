<?php

use Phalcon\Mvc\Micro\Collection as MicroCollection;

/**
    Clase que se encarga de cargar las rutas para un determinado controlador
*/
class EmpresaRouterDefiner
{
    private static $instance = NULL;
    private $logger = NULL;
    private $app = NULL;
    private $controller = NULL;
    
    /**
        Recibe una instancia del controlador para el que se requieren crear las rutas
        Con la instancia del controlador obtiene una referencia del logger y de la miroapp
    */
    public function __construct ($controller)
    {
        $this->logger = $controller->logger;
        $this->app = $controller->app;
        $this->controller = $controller;
        $this->init();
    }

    /**
        Funcion inicial que registra todas las rutas del controlador al que pertenece esta clase
    */
    public function init()
    {
        $this->logger->debug("Definig all routes for Empresa");
        
        $mc = new MicroCollection();
        
        $mc->setHandler($this->controller);
        $mc->setPrefix('/empresa');
        
        $mc->get('/', "listar");
        
        $mc->get('/{id:[0-9a-z]+}', "get");
        
        $mc->post('/', "nuevo");

        $mc->put('/{id:[0-9a-z]+}', "modificar");

        $mc->delete('/{id:[0-9a-z]+}', "eliminar");
        
        $this->app->mount($mc);
    }
    
    public static function getInstance($controller){
        if(is_null(self::$instance))
            self::$instance = new EmpresaRouterDefiner($controller);
        return self::$instance;
    }
    
}

