<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Validator\StringLength;

/**
    Clase que representa a la entidad Usuario
*/
class Usuario extends BaseModel
{
    protected $id;
    // siempre una linea en blanco entre variables
    protected $nombre;
    
    protected $apellido;
    
    protected $fecha_nacimiento;
    
    protected $email;
    
    protected $clave;
    
    protected $cedula;
    
    protected $empresa_id;
    
    protected $fecha_creacion;
    
    protected $creado_por;
    
    protected $actualizado_por;

    protected $fecha_actualizacion;
    
    protected $estatus;


    public function initialize()
    {
        $this->setTable("Usuario");
        $this->belongsTo("empresa_id", "Empresa", "id", array('alias' => 'Empresa'));
        $this->skipAttributesOnCreate(array('actualizado_por', 'fecha_actualizacion'));
        $this->skipAttributesOnUpdate(array('creado_por', 'fecha_creacion'));
        /**
            Usar este comportamiento en las entidades que SOLO tiene softdelete
        
        $this->addBehavior(
            new SoftDelete(
                array(
                    'field' => 'status', // campo a cambiar de estatus
                    'value' => ICommons::STATUS_DELETED // valor a asignar
                )
            )
        );
        */
    }
    
    public function setTable($table)
    {
        $this->setSource($table);
    }

    public function onConstruct()
    {
        $this->setNombre("nombre por defecto"); 
        // ejemplo de uso de onConstruct. Se ejecuta en el momento de creacion de cada instancia
    }
    
    public function beforeCreate()
    {
        // Seteamos el momento de creacion del registro
        $this->fecha_creacion = date_format(new DateTime(), ICommons::DATE_FORMAT);
        $this->fecha_actualizacion = date_format(new DateTime(), ICommons::DATE_FORMAT);
    }

    public function beforeUpdate()
    {
        /** 
        Seteamos el momento de actualizacion del registro
        se debe usar el campo correspondiente
        */
         $this->fecha_actualizacion = date_format(new DateTime(), ICommons::DATE_FORMAT);
    }

    public function afterFetch()
    {
        // Alguna conversion importante antes de devolver los valores
    }
    
    public function beforeValidation()
    {
        // Aqui se añaden validaciones que si no se cumplen pueden interrumpir el proceso de insert/update
    }
    
    public function validation()
    {
       /* $this->validate(
            new MaxMinValidator( // uso de validador personalizado
                array(
                    "field"     => "tipo",
                    "min"       => 0,
                    "max"       => 999999999999
                )
            )
        );*/

        $this->validate(
            new Uniqueness(
                array(
                    "field"     => "id",
                    "message"   => "Error: El campo id debe ser unico"
                )
            )
        );
        
        $this->validate(
            new Uniqueness(
                array(
                    "field"     => "cedula",
                    "message"   => "Error: El campo cedula debe ser unico"
                )
            )
        );
        
        $this->validate(
            new PresenceOf(
                array(
                    "field"     => "nombre",
                    "message"   => "Error: El campo nombre no puede estar vacio"
                )
        ));
        
        $this->validate(
            new PresenceOf(
                array(
                    "field"     => "apellido",
                    "message"   => "Error: El campo apellido no puede estar vacio"
                )
        ));
        
        $this->validate(
            new PresenceOf(
                array(
                    "field"     => "cedula",
                    "message"   => "Error: El campo cedula no puede estar vacio"
                )
        ));
        
        $this->validate(
            new PresenceOf(
                array(
                    "field"     => "empresa_id",
                    "message"   => "Error: Todo usuario debe pertenecer a una empresa."
                )
        ));
        
        /*$this->validate(
            new StringLength(
                array(
                    "field" => 'marca',
                    'max' => 10,
                    'min' => 1,
                    'messageMaximum' => 'Error: El campo marca no puede tener mas de 10 caracteres',
                    'messageMinimum' => 'Error: El campo marca no puede tener menos de 1 caractere'
        )));*/


        return $this->validationHasFailed() != true;
    }
    
    /**
        Funcion que implementa la funcion abstracta padre
        y convierte las propiedades de la clase en un array
    */
    public function meToJson()
    {
        $data[] = array(
            'id'   => $this->id,
            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'email' => $this->email,
            'clave' => $this->clave,
            'cedula' => $this->cedula,
            'empresa_id' => (object)$this->getEmpresa()->meToJsonLite()[0],
            'fecha_creacion' => $this->fecha_creacion,
            'creado_por' => $this->creado_por,
            'actualizado_por' => $this->actualizado_por,
            'fecha_actualizacion' => $this->fecha_actualizacion,
            'estatus' => $this->estatus,
        );
        return $data;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    public function getApellido()
    {
        return $this->apellido;
    }
    public function setFechaNacimiento($fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    public function getFechaNacimiento()
    {
        return $this->fecha_nacimiento;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setClave($clave)
    {
        $this->clave = $clave;
    }
    public function getClave()
    {
        return $this->clave;
    }
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;
    }
    public function getCedula()
    {
        return $this->cedula;
    }
    public function setEmpresaID($empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }
    public function getEmpresaID()
    {
        return $this->empresa_id;
    }
    public function setFechaCreacion($fecha_creacion)
    {
        $this->fecha_creacion = $fecha_creacion;
    }

    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }
    public function setFechaActualizacion($fecha_actualizacion)
    {
        $this->fecha_actualizacion = $fecha_actualizacion;
    }

    public function getFechaActualizacion()
    {
        return $this->fecha_actualizacion;
    }
    
    public function setCreado_Por($creado_por)
    {
        $this->creado_por = $creado_por;
    }

    public function getCreado_Por()
    {
        return $this->creado_por;
    }
    
    public function setActualizado_Por($actualizado_por)
    {
        $this->actualizado_por = $actualizado_por;
    }

    public function getActualizado_Por()
    {
        return $this->actualizado_por;
    }
    
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;
    }

    public function getEstatus()
    {
        return $this->estatus;
    }
    /**
        Funcion toString para imprimir el objeto por consola o en logs
    */
    public function __toString()
    {
        $str = '';
        foreach($this as $key => $value) { // se eliminan de la impresion las varibales tipo FactoryDefault, Manager y los array
            if (!($value instanceof  Phalcon\Di\FactoryDefault) and !($value instanceof Phalcon\Mvc\Model\Manager) and !($value instanceof Phalcon\Mvc\Model\MetaData\Memory) and !is_array($value) ) {
                $str .= "$key => $value\n";
            }
        }
        return $str;
    }
}