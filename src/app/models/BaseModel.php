<?php

use Phalcon\Mvc\Model;

/**
    Clase padre que tiene gestiones comunes de Model
*/
abstract class BaseModel extends Model 
{
    /**
        Funcion que carga un json en las propiedades de la clase
    */
    public function jsonToMe($jsonData, &$me)
    {
        // mecanismo dinamico de mapeo del objeto recibido al objeto actual
        foreach($jsonData as $obj => $objValue) 
        { 
            foreach($me as $gen => $genValue) 
            {
                $mapped = false;
                if (strcmp ($gen , $obj ) == 0) 
                {
                    $me->{$gen} = $objValue;
                    $mapped = true;
                }
                if ($mapped) 
                {
                    continue;
                }
            }
        }
        // return $me;
    }
    
    /**
        Funcion abstracta que obliga a implementar la conversion a json 
        de las propiedades de la clase
    */
    abstract public function meToJson();
    
    /**
        Funcion abstracta que permite externalizar la selección de la tabla
    */
    abstract public function setTable($table);

}