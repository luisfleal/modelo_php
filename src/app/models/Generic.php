<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\PresenceOf,
    Phalcon\Mvc\Model\Validator\StringLength;

/**
    Clase que representa a la entidad Generic
*/
class Generic extends BaseModel
{
    protected $id;
    // siempre una linea en blanco entre variables
    protected $nombre;
    
    protected $tipo;
    
    protected $fecha;
    
    protected $marca;

    public function initialize()
    {
        $this->setTable("genericos");

        /**
            Usar este comportamiento en las entidades que SOLO tiene softdelete
        
        $this->addBehavior(
            new SoftDelete(
                array(
                    'field' => 'status', // campo a cambiar de estatus
                    'value' => ICommons::STATUS_DELETED // valor a asignar
                )
            )
        );
        */
    }
    
    public function setTable($table)
    {
        $this->setSource($table);
    }

    public function onConstruct()
    {
        $this->setNombre("nombre por defecto"); 
        // ejemplo de uso de onConstruct. Se ejecuta en el momento de creacion de cada instancia
    }
    
    public function beforeCreate()
    {
        // Seteamos el momento de creacion del registro
        $this->fecha = date_format(new DateTime(), ICommons::DATE_FORMAT);
    }

    public function beforeUpdate()
    {
        /** 
        Seteamos el momento de actualizacion del registro
        se debe usar el campo correspondiente
        */
        // $this->fecha = date_format(new DateTime(), ICommons::DATE_FORMAT);
    }

    public function afterFetch()
    {
        // Alguna conversion importante antes de devolver los valores
    }
    
    public function beforeValidation()
    {
        // Aqui se añaden validaciones que si no se cumplen pueden interrumpir el proceso de insert/update
    }
    
    public function validation()
    {
        $this->validate(
            new MaxMinValidator( // uso de validador personalizado
                array(
                    "field"     => "tipo",
                    "min"       => 0,
                    "max"       => 999999999999
                )
            )
        );

        $this->validate(
            new Uniqueness(
                array(
                    "field"     => "id",
                    "message"   => "Error: El campo id debe ser unico"
                )
            )
        );
        
        $this->validate(
            new Uniqueness(
                array(
                    "field"     => "nombre",
                    "message"   => "Error: El campo nombre debe ser unico"
                )
            )
        );
        
        $this->validate(
            new PresenceOf(
                array(
                    "field"     => "nombre",
                    "message"   => "Error: El campo nombre no puede estar vacio"
                )
        ));
        
        $this->validate(
            new StringLength(
                array(
                    "field" => 'marca',
                    'max' => 10,
                    'min' => 1,
                    'messageMaximum' => 'Error: El campo marca no puede tener mas de 10 caracteres',
                    'messageMinimum' => 'Error: El campo marca no puede tener menos de 1 caractere'
        )));


        return $this->validationHasFailed() != true;
    }
    
    /**
        Funcion que implementa la funcion abstracta padre
        y convierte las propiedades de la clase en un array
    */
    public function meToJson()
    {
        $data[] = array(
            'id'   => $this->id,
            'nombre' => $this->nombre,
            'fecha' => $this->fecha,
            'tipo' => $this->tipo,
            'marca' => $this->marca,
        );
        return $data;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function setTipo($tipo)
    {
        if ($tipo < 1) {
            // podemos lanzar una excepcion para que se rompa el proceso y sea capturada por el nivel superior
            throw new \InvalidArgumentException('El tipo debe ser mayor que cero');
        }
        
        $this->tipo = $tipo;
    }

    public function getTipo()
    {
        return $this->tipo;
    }
    
    public function getFecha()
    {
        return $this->fecha;
    }
    
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getMarca()
    {
        return $this->marca;
    }
    
    public function setMarca($marca)
    {
        if (strlen($marca) < 1) {
            // podemos lanzar una excepcion para que se rompa el proceso y sea capturada por el nivel superior
            throw new \InvalidArgumentException('La marca debe tener valor');
        }
        
        $this->marca = $marca;
    }
    
    /**
        Funcion toString para imprimir el objeto por consola o en logs
    */
    public function __toString()
    {
        $str = '';
        foreach($this as $key => $value) { // se eliminan de la impresion las varibales tipo FactoryDefault, Manager y los array
            if (!($value instanceof  Phalcon\Di\FactoryDefault) and !($value instanceof Phalcon\Mvc\Model\Manager) and !is_array($value) ) {
                $str .= "$key => $value\n";
            }
        }
        return $str;
    }
}