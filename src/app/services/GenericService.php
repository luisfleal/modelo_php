<?php

use Phalcon\Mvc\User\Component;

/**
    Clase de tipo Component que implementa la interfaz que define su comportamiento
*/
class GenericService extends Component implements IGenericService
{

    private static $instance = NULL;
    private $dao = NULL;
    private $logger = NULL;

    /**
        Constructor que recibe una referencia del logger
        Internamiente obtiene de la nube de dependencias la referencia del dao que requiere usar
    */
    public function __construct($logger, IGenericDao $dao) {
        $this->logger = $logger;
        $this->dao = $dao;
    }

    public function listar($filter){
        $this->logger->debug("debug on service");
                
        if (!empty($filter)) 
        {
            // aqui algunas validaciones de los filtros por ejemplo:

            if (array_key_exists("order", $filter))
                if (strpos($filter["order"],'-') !== false)  // para revisar si el campo requiere filtro descendente
                {
                    $filter["order"] = substr($filter["order"], 1) . " DESC";
                    $this->logger->debug($filter["order"]);
                }
        }
                
        $data = $this->dao->listar($filter);
        return $data->toArray();
    }
    
    public function nuevo($object){
        $generic = new Generic();
        $generic->jsonToMe($object, $generic);
        $result = $this->dao->nuevo($generic);
        if ($result and strpos($result,ICommons::ERROR) === false) 
        {
            return $generic->meToJson()[0];
        } else 
        {
            return $result;
        }
        
    }

    public function modificar($id, $object)
    {
        $generic = $this->dao->findByPK($id);
        if ($generic) 
        {
            $generic->jsonToMe($object, $generic);
            $result = $this->dao->modificar($generic);
            if ($result === true)
            {
                return $this->dao->findByPK($id)->meToJson()[0];
                
            } else 
            {
                return $result;
            }
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function eliminar($id)
    {
        $generic = $this->dao->findByPK($id);
        if ($generic) 
        {
            $result = $this->dao->eliminar($generic);
            return $result;
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function getByPK($id){
        $generic = $this->dao->findByPK($id);
        if (empty($generic))
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        } else 
        {
            $generic = $generic->meToJson()[0];
        }
        return $generic;
    }

    public static function getInstance($logger, IGenericDao $dao){
        if(is_null(self::$instance))
            self::$instance = new GenericService($logger, $dao);
        return self::$instance;
    }

}
?>
