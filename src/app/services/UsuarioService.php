<?php

use Phalcon\Mvc\User\Component;

/**
    Clase de tipo Component que implementa la interfaz que define su comportamiento
*/
class UsuarioService extends Component implements IUsuarioService
{

    private static $instance = NULL;
    private $dao = NULL;
    private $logger = NULL;

    /**
        Constructor que recibe una referencia del logger
        Internamiente obtiene de la nube de dependencias la referencia del dao que requiere usar
    */
    public function __construct($logger, IUsuarioDao $dao) {
        $this->logger = $logger;
        $this->dao = $dao;
    }

    public function listar($filter){
        $this->logger->debug("debug on service");
                
        if (!empty($filter)) 
        {
            // aqui algunas validaciones de los filtros por ejemplo:

            if (array_key_exists("order", $filter))
                if (strpos($filter["order"],'-') !== false)  // para revisar si el campo requiere filtro descendente
                {
                    $filter["order"] = substr($filter["order"], 1) . " DESC";
                    $this->logger->debug($filter["order"]);
                }
        }
                
        $data = $this->dao->listar($filter);
        return $data->toArray();
    }
    
    public function nuevo($object){
        $usuario = new Usuario();
        $usuario->jsonToMe($object, $usuario);
        $result = $this->dao->nuevo($usuario);
        if ($result and strpos($result,ICommons::ERROR) === false) 
        {
            return $usuario->meToJson()[0];
        } else 
        {
            return $result;
        }
        
    }

    public function modificar($id, $object)
    {
        $usuario = $this->dao->findByPK($id);
        if (is_object($usuario)) 
        {
            $usuario->jsonToMe($object, $usuario);
            $result = $this->dao->modificar($usuario);
            if ($result === true)
            {
                return $this->dao->findByPK($id)->meToJson()[0];
                
            } else 
            {
                return $result;
            }
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function eliminar($id)
    {
        $usuario = $this->dao->findByPK($id);
        if (is_object($usuario)) 
        {
            $result = $this->dao->eliminar($usuario);
            return $result;
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function getByPK($id){
        $usuario = $this->dao->findByPK($id);
        if (empty($usuario))
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        } else 
        {
            $usuario = $usuario->meToJson()[0];
        }
        return $usuario;
    }

    public static function getInstance($logger, IUsuarioDao $dao){
        if(is_null(self::$instance))
            self::$instance = new UsuarioService($logger, $dao);
        return self::$instance;
    }

}
?>
