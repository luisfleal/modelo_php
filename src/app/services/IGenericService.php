<?php

interface IGenericService 
{

    public function listar($filter);

    public function getByPK($id);

    public function nuevo($data);

    public function modificar($id, $object);

    public function eliminar($id);

}
