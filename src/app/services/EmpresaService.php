<?php

use Phalcon\Mvc\User\Component;

/**
    Clase de tipo Component que implementa la interfaz que define su comportamiento
*/
class EmpresaService extends Component implements IEmpresaService
{

    private static $instance = NULL;
    private $dao = NULL;
    private $logger = NULL;

    /**
        Constructor que recibe una referencia del logger
        Internamiente obtiene de la nube de dependencias la referencia del dao que requiere usar
    */
    public function __construct($logger, IEmpresaDao $dao) {
        $this->logger = $logger;
        $this->dao = $dao;
    }

    public function listar($filter){
        $this->logger->debug("debug on service");
                
        if (!empty($filter)) 
        {
            // aqui algunas validaciones de los filtros por ejemplo:

            if (array_key_exists("order", $filter))
                if (strpos($filter["order"],'-') !== false)  // para revisar si el campo requiere filtro descendente
                {
                    $filter["order"] = substr($filter["order"], 1) . " DESC";
                    $this->logger->debug($filter["order"]);
                }
        }
                
        $data = $this->dao->listar($filter);
        return $data->toArray();
    }
    
    public function nuevo($object){
        $empresa = new Empresa();
        $empresa->jsonToMe($object, $empresa);
        $result = $this->dao->nuevo($empresa);
        if ($result and strpos($result,ICommons::ERROR) === false) 
        {
            return $empresa->meToJson()[0];
        } else 
        {
            return $result;
        }
        
    }

    public function modificar($id, $object)
    {
        $empresa = $this->dao->findByPK($id);
        if (is_object($empresa)) 
        {
            $empresa->jsonToMe($object, $empresa);
            $result = $this->dao->modificar($empresa);
            if ($result === true)
            {
                return $this->dao->findByPK($id)->meToJson()[0];
                
            } else 
            {
                return $result;
            }
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function eliminar($id)
    {
        $empresa = $this->dao->findByPK($id);
        if (is_object($empresa)) 
        {
            $result = $this->dao->eliminar($empresa);
            return $result;
        } else 
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        }
    }
    
    public function getByPK($id){
        $empresa = $this->dao->findByPK($id);
        if (empty($empresa))
        {
            return ICommons::INVALID_RECORD_NOT_EXIST;
        } else 
        {
            $empresa = $empresa->meToJson()[0];
        }
        return $empresa;
    }

    public static function getInstance($logger, IEmpresaDao $dao){
        if(is_null(self::$instance))
            self::$instance = new EmpresaService($logger, $dao);
        return self::$instance;
    }

}
?>
