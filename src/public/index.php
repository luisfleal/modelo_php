<?php

use Phalcon\Loader,
    Phalcon\DI\FactoryDefault,
    Phalcon\Mvc\Micro;

/**
Inclusion de interfaz con constantes bases para la plataforma
*/
require_once("../app/commons/ICommons.php");    

/**
Clase principal para el arranque del sistema. Se instancia al final del archivo
*/
class Main
{
    private static $instance = NULL;
    public $di = NULL;
    public $app = NULL;
    
    // El Inyector de dependencias se carga solo una vez al instancias
    public function __construct()
    {
        $this->di = new FactoryDefault();
        $this->init();
    }
    
    /**
        Funcion inicial que carga los valores iniciales del sistema y desencadena
        la carga de la clase Bootstrap
    */
    private function init()
    {
    
        try {
            
            $loader = new Loader();
            $loader->registerDirs(array(
                '../app/bootstrap/'
            ))->register();
            /** 
            Solo se carga el directorio que tiene la clase Bootstrap del sistema
            Esto permite aislar las clases de negocio y aumentar la seguridad
            */
            

            $this->app = new Micro($this->di);

            $this->app->get('/',function(){
                $this->app->response->setStatusCode(ICommons::HTTP_200, ICommons::HTTP_200_MSG);
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
                $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_200_MSG, ICommons::RESPONSE_MENSAJE => ICommons::RESPONSE_WELCOME_API_REST));
                // Se usaran constantes para los valores de retorno, headers y mensajes
                $this->app->response->send();
            });
            

            $this->app->notFound(function () {
                $this->app->response->setStatusCode(ICommons::HTTP_404, ICommons::HTTP_404_MSG);
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
                $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_404_MSG, ICommons::RESPONSE_MENSAJE => ICommons::RESPONSE_RUTA_INEXISTENTE));
                $this->app->response->send();
                
            });
            
            $this->app->error(function ($exception) {
                $this->app->response->setStatusCode(ICommons::HTTP_500, ICommons::HTTP_500_MSG);
                $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
                $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::HTTP_500_MSG, ICommons::RESPONSE_MENSAJE => $exception->getMessage()));
                $this->app->response->send();
            });

            // Instancia de la clase Bootstrap
            Bootstrap::getInstance($this->app);

            $this->app->handle();

        } 
        catch (\Phalcon\Exception $e) 
        { 
            // Todas las respuestas, incluso las de error, serán json
            $this->app->response->setStatusCode(ICommons::HTTP_500, ICommons::HTTP_500_MSG);
            $this->app->response->setHeader(ICommons::HEADER_CONTENT_TYPE, ICommons::HEADER_CONTENT_JSON);
            $this->app->response->setJsonContent(array(ICommons::RESPONSE_RESULTADO => ICommons::RESPONSE_ERROR, ICommons::RESPONSE_MENSAJE => $e->getMessage()));
            $this->app->response->send();
        }
    }
    
    public static function getInstance(){
        if(is_null(self::$instance))
            self::$instance = new Main();
        return self::$instance;
    }
    

}

// Instancia inicial
Main::getInstance();

?>
