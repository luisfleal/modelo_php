<?php
/*
try
{
    xdebug_get_code_coverage();
    require __DIR__.'/../c3.php';
} catch (Exception $ex) 
{
    error_log($ex->getMessage(),0);
}
*/
#require_once('../test/coverage_config.php');
require_once('../public/index.php');

use Phalcon\Logger,
    Phalcon\Logger\Adapter\File as FileAdapter,
    \Codeception\Util\Stub;


class UsuarioTest extends \Codeception\TestCase\Test
{
    private $dao;
    private $logger;
    private $nombreModel = "Usuario";
    private $newId = 0;
    private $noExistId = 12345;
    private $usuario = NULL; // objeto stub de creacion o actualizacion
    private $genericFound = NULL; // objeto stub para findFirst
    private $DMLReturn = true; // define si se crea o actualiza realmente en la BD o no
    
    public function __construct() 
    {
        $this->logger = Bootstrap::getExistInstance()->getLogger();

        $this->usuario = new Usuario();
        $this->usuario->jsonToMe(array(
            'id' => $this->newId,
            'nombre' => 'nombre_prueba_1',
            'apellido' => 'apellido_prueba_1',
            'fecha_nacimiento' => '1990-01-01',
            'email' => 'email_test@example.com',
            'clave' => '1234',
            'cedula' => 'V910111213',
            'empresa_id' => '0',
            'creado_por' => '1',
            'actualizado_por' => '1',
            'estatus' => '1'
            ), $this->usuario);
    }
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testConstructor()
    {
        $result = new UsuarioDao($this->logger);
        $this->assertTrue(get_class($result) === "UsuarioDao");
        $this->assertTrue(is_object($result));
    }

    public function testGetInstance()
    {
        $result = UsuarioDao::getInstance($this->logger);
        $this->assertTrue(get_class($result) === "UsuarioDao");
        $this->assertTrue(is_object($result));   
        return $result;     
    }
    /**
    * @depends testGetInstance
    */
    public function testNuevoNotOkInvalidNombre($dao)
    {
        $dao->eliminar($this->usuario);
        $this->usuario->setNombre(NULL);
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->nuevo($this->usuario));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testNuevoNotOkInvalidApellido($dao)
    {
        $dao->eliminar($this->usuario);
        $this->usuario->setApellido(NULL);
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->nuevo($this->usuario));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testNuevoNotOkInvalidCedula($dao)
    {
        $dao->eliminar($this->usuario);
        $this->usuario->setCedula(NULL);
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->nuevo($this->usuario));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testNuevoNotOkInvalidDateCreacion($dao)
    {
        $dao->eliminar($this->usuario);
        $this->usuario->setFechaCreacion("testNuevoNotOkInvalidDateCreacion");
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->nuevo($this->usuario));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testNuevoNotOkInvalidDateActualizacion($dao)
    {
        $dao->eliminar($this->usuario);
        $this->usuario->setFechaActualizacion("testNuevoNotOkInvalidDateActualizacion");
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->nuevo($this->usuario));
    }
    
     
    
    /**
    * @depends testGetInstance
    * @depends testNuevoOk
    */
    public function testNuevoNotOkBecauseExist($dao, $usuario)
    {
        $this->assertContains(ICommons::INVALID_SAVE_RECORD_EXIST,$dao->nuevo($usuario));
    }

    /**
    * @depends testGetInstance
    */
    public function testFindByPKDoesntExist($dao)
    {
        $this->assertFalse($dao->findByPK($this->noExistId));
        $dao->nuevo($this->usuario);
    }

    /**
    * @depends testGetInstance
    */
    public function testFindByPKExist($dao)
    {
        $result = $dao->findByPK($this->newId);
        $this->assertNotNull($result);
        $this->assertNotEquals(false, $result);
        
    }
    
    /**
    * @depends testGetInstance
    */
    public function testFindByPKIsObject($dao)
    {
        $this->assertTrue(is_object($dao->findByPK($this->newId)));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testFindByPKIsGeneric($dao)
    {
        $this->assertInstanceOf($this->nombreModel, $dao->findByPK($this->newId));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testFindByPKUnexpectedError($dao)
    {
        $this->usuario->setTable("tabla_inexistente");
        $result = $dao->findByPK($this->newId);
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$result);
        $this->usuario->setTable("Usuario");
    }
    
    /**
    * @depends testGetInstance
    */
    public function testListarOk($dao)
    {
        $result = $dao->listar(NULL);
        $this->logger->debug(gettype($result));
        $this->assertEquals("object", gettype($result));
        $data = $result->toArray();
        $this->assertFalse(empty($result));
    }
    
    /**
    * @depends testGetInstance
    */
    public function testListarOkFilterNombre($dao)
    {
        $filter = array("nombre" => "nombre_prueba_1");
        $result = $dao->listar($filter);
        $data = $result->toArray();
        $this->assertFalse(empty($data));
        $this->assertTrue(in_array("nombre", $data[0]));  
    }
    
    /**
    * @depends testGetInstance
    */
    public function testModificarOkObjetoCompleto($dao) 
    {
        $this->usuario->jsonToMe(array(
            'nombre' => 'nombre_prueba_1_modificado',
            'apellido' => 'apellido_prueba_1_modificado',
            'fecha_nacimiento' => '1987-01-01',
            'email' => 'email_test_modificado@example.com',
            'clave' => '12345',
            'cedula' => 'V91011121314',
            'empresa_id' => '0',
            'creado_por' => '1',
            'actualizado_por' => '1',
            'estatus' => '1'           
            ), $this->usuario);
        $this->assertTrue($dao->modificar($this->usuario));
        $result = $dao->findByPK($this->newId);
        $this->assertEquals($this->usuario->nombre, $result->nombre);
        $this->assertEquals($this->usuario->apellido, $result->apellido);
        $this->assertEquals($this->usuario->fecha_nacimiento, $result->fecha_nacimiento);
        $this->assertEquals($this->usuario->clave, $result->clave);
        $this->assertEquals($this->usuario->cedula, $result->cedula);
    }
    
    /**
    * @depends testGetInstance
    */
    public function testModificarOkSoloNombre($dao) 
    {
        $this->usuario->jsonToMe(array(
            'nombre' => 'solo nombre'           
            ), $this->usuario);
        $this->assertTrue($dao->modificar($this->usuario));
        $result = $dao->findByPK($this->newId);
        $this->assertEquals($this->usuario->nombre, $result->nombre);
    }
    
    /**
    * @depends testGetInstance
    */
    public function testModificarNotOkNoExiste($dao) 
    {
        $this->usuario->setId("NO EXISTE");
        $this->assertContains(ICommons::UNEXPECTED_ERROR,$dao->modificar($this->usuario));

    }
    
    
    /**
    * @depends testGetInstance
    */
    public function testEliminarOk($dao) 
    {
        $this->assertTrue($dao->eliminar($this->usuario));
    }
    

}
