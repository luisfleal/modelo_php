##Arquitectura de Frontend: 

###Controladores
 - Los Controladores a usar son para nuestras 2 entidades, EmpresaController.php y UsuarioController.php
 - Ambos sun utilizados para conectarse a la base de datos y hacer las query requeridas.
###Servicios
 - Los Servicios obtienen la data JSON enviada por el DAO y la interpretan para uso del controlador y viceversa, utilizando los metodos jsonToMe() y meToJson() respectivamente.
 - Cada Servicio implementa su interfaz, por lo tanto se tienen IEmpresaService.php e IUsuarioService.php, que son las interfaces de EmpresaService.php e UsuarioService.php.
###Daos
 - Los DAOs, Data Access Object, proveen una interfaz a la que el frontend se conecta antes de ir a los datos en si.
 - Cada DAO implementa una intefaz propia de sus metodos.
 - Es un DAO por cada entidad, asi que tendriamos UsuarioDao.php y EmpresaDao.php, junto a sus interfaces IUsuarioDao.php e IEmpresaDao.php
###Inyectores
 - Los inyectores crean las instancias de los objetos DAO y los objetos Servicios, que acceden a los Controladores cargados en Bootstrap directamente.
 - Los inyectores son llamados en Bootstrap, hay un inyector de DAOs y un inyector de Servicios.
 - En dichos inyectores se debe llamar cada DAO y cada servicio por separado. 
 - Dado que el servicio es el que se conecta al DAO del lado del servidor, el DAO es el que se inyecta primero y el servicio llama a esa instancia de DAO al ser inyectado.
